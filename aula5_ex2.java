package pc;

import java.util.Scanner;

public class aula5_ex2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num;
		int[] gasosa;
		gasosa = new int[4];
		
		System.out.println("1.Álcool 2.Gasolina 3.Diesel 4.Fim\nTipo de combustível: ");
		
		do {
			num = sc.nextInt();
			if (num < 4 && num > 0) {
				gasosa[num-1] += 1;
			}

		} while (num != 4);
		
		System.out.printf("Álcool: %d\nGasolina: %d\nDiesel: %d\n", gasosa[0], gasosa[1], gasosa[2]);
		System.out.println("Fim do código!");
		sc.close();	
		}
}
