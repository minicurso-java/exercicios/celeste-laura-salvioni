package pc;

import java.util.Scanner;

public class aula6_ex5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int N = 5;
		int velha = 0;
		int indice_velha = -1;
		String nome_velha = " ";
		
		String[] nomes = new String[N];
		int[] idades = new int[N];
		
		for (int i = 0; i < N; i++) {
			System.out.printf("Nome %d: ", i+1);
			nomes[i] = sc.next();
			System.out.printf("Idade %d: ", i+1);
			idades[i] = sc.nextInt();
			
			if (idades[i] > velha) {
				velha = idades[i];
				indice_velha = i;
				nome_velha = nomes[i];
			}
		}
	
		System.out.println("Pessoa mais velha: " + nome_velha + " // Indice: " + indice_velha);
		sc.close();
	}
}
