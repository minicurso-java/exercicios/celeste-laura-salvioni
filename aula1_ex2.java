package main;

import java.util.Scanner;

/*Fazer um programa para ler quatro valores inteiros A, B, C e D. A seguir, 
calcule e mostre a diferença do produto de A e B pelo produto de C e D segundo a fórmula: 
DIFERENCA = (A * B - C * D).

*/

public class aula1_ex2 {
	public static void main() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("A: ");
		double a = sc.nextDouble();
		System.out.println("B: ");
		double b = sc.nextDouble();
		System.out.println("C: ");
		double c = sc.nextDouble();
		System.out.println("D: ");
		double d = sc.nextDouble();
		
		double calculo = (a*b)-(c*d);
		
		System.out.println("resultado: "+ calculo);
		sc.close();
	}
}