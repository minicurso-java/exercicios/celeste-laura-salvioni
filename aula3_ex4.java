package main;

import java.util.Scanner;
/*
Elabore uma função que receba três notas de um aluno como parâmetro e uma letra.
Se a letra for ‘A’ / ‘a’, a função deve calcular a média aritmética das notas do aluno;
 se a letra for ‘P’ / ‘p’, deverá calcular a média ponderada, com pesos 5, 3 e 2, respectivamente com as notas 1, 2, 3.
 Retorne a média calculada para o programa principal.
 */

public class aula3_ex4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double[] notas = new double[3];

        int i;
        for (i = 1; i < 4; i++){
            System.out.printf("Nota %d: ", i);
            notas[i-1] = sc.nextDouble();
        }

        System.out.println("Tipo de média (aritmética/ponderada): ");
        char tipo = sc.next().charAt(0);

        double media = funcMedia(notas[0], notas[1], notas[2], tipo);
        System.out.printf("Media: %.2f", media);

    }
    public static double funcMedia (double n1, double n2, double n3,char letra){

        if (letra == 'A'|| letra == 'a') {
            return ((n1 + n2 + n3) / 3);
        } else if (letra == 'P'|| letra == 'p') {
            return ((n1 * 5) + (n2 * 3) + (n3 * 2)) / 10;
        } else {
            return 0.0;
        }
    }
}
