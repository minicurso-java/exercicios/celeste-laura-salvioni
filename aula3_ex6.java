package main;

import java.util.Scanner;

public class aula3_ex6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Digite aqui sua frase: ");
        String frase = sc.nextLine();

        String f1 = frase.toLowerCase();
        f1 = f1.replace("a", "");
        f1 = f1.replace("e", "");
        f1 = f1.replace("i", "");
        f1 = f1.replace("o", "");
        f1 = f1.replace("u", "");

        System.out.println(f1);
        sc.close();
    }
}
