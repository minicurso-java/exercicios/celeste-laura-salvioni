package main;

import java.util.Locale;
import java.util.Scanner;

public class aula1_ex1 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite o valor de A, B e C, respectivamente:");
		double A = sc.nextDouble();
		double B = sc.nextDouble();
		double C = sc.nextDouble();
		
		double areaTri = (A*C)/2;
		double areaCir = 3.14159 * (C*C);
		double areaTra = ((A+B)*C)/2;
		double areaQua = B*B;
		double areaRet = A*B;
		
		System.out.printf("Triângulo: %2f\nCírculo: %2f\nTrapézio: %2f\nQuadrado: %2f\nRetângulo: %2f\n", areaTri, areaCir, areaTra, areaQua, areaRet);
		
		sc.close();
	}

}
