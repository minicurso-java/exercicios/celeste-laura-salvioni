package main;

import java.util.Scanner;

public class aula2_ex8 {
	public static void main() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("x: ");
		double x = sc.nextDouble();
		System.out.println("y: ");
		double y = sc.nextDouble();
		String quadrante = "";
		
		if (x == 0 || y == 0) {
			quadrante = "Origem";
		} else if (x > 0 || y > 0) {
			quadrante = "Q1";
		} else if (x < 0 || y > 0) {
			quadrante = "Q2";
		} else if (x < 0 || y < 0) {
			quadrante = "Q2";
		} else {
			quadrante = "Q4";
		}
		
		System.out.println(quadrante);
		sc.close();
	}
}

