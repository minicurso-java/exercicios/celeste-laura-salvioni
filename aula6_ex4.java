package pc;

import java.util.Scanner;

public class aula6_ex4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int N = 5;
		double maior = -1;
		int indice = -1;
		
		double[] vetor = new double[N];
		
		for (int i = 0; i < N; i++) {
			System.out.printf("Número %d: ", i+1);
			vetor[i] = sc.nextDouble();
			if (vetor[i] > maior) {
				maior = vetor[i];
				indice = i;
			}
		}
		
		System.out.println("Maior num: " + maior + " // " + "Ìndice: " + indice);
		sc.close();
	}
}
