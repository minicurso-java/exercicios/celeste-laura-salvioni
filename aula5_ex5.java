package pc;

import java.util.Scanner;

public class aula5_ex5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char resposta = 's';
		
		while (resposta == 's') {
			System.out.print("Temperatura em Celsius: ");
			double temp = sc.nextDouble();
			
			double novaTemp = (temp*1.8) + 32;
			
			System.out.println("Equivalente em Pahrenheit: " + novaTemp);
			
			System.out.println("Deseja repetir o programa? s/n");
			resposta = sc.next().charAt(0);
		}
		System.out.println("Fim do programa.");
	sc.close();	
	}
}
